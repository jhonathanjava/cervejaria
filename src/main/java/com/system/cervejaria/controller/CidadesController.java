package com.system.cervejaria.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/cidades")
@Controller
public class CidadesController {

	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo() {
		return "/cidades/CadastroCidades";
	}
}
