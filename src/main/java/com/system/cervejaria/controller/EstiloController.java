package com.system.cervejaria.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/estilos")
@Controller
public class EstiloController {

	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo() {
		return "/estilo/CadastroEstilos";
	}
}
