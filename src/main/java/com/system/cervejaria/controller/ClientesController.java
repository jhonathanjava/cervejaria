package com.system.cervejaria.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/clientes")
@Controller
public class ClientesController {

	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public String novo() {
		return "clientes/CadastroClientes";
	}
}
